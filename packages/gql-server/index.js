var express = require("express");
var cors = require("cors");
var axios = require("axios");
var { graphqlHTTP } = require("express-graphql");
var { buildSchema } = require("graphql");

const PORT = process.env.PORT || 4000;

const quotes = [
  {
    id: 1,
    quote:
      "Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.",
    name: "Albert Einstein",
  },
  {
    id: 2,
    quote: "Be the change that you wish to see in the world.",
    name: "Mahatma Gandhi",
  },
  {
    id: 3,
    quote:
      "In three words I can sum up everything I've learned about life: it goes on.",
    name: "Robert Frost",
  },
  {
    id: 4,
    quote:
      "To live is the rarest thing in the world. Most people exist, that is all.",
    name: "Oscar Wilde",
  },
  {
    id: 5,
    quote:
      "Live as if you were to die tomorrow. Learn as if you were to live forever.",
    name: "Mahatma Gandhi",
  },
  {
    id: 6,
    quote:
      "I am so clever that sometimes I don't understand a single word of what I am saying.",
    name: "Oscar Wilde",
  },
  {
    id: 7,
    quote:
      "There are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.",
    name: "Albert Einstein",
  },
  {
    id: 8,
    quote: "We are all in the gutter, but some of us are looking at the stars.",
    name: "Oscar Wilde",
  },
  {
    id: 9,
    quote:
      "The fool doth think he is wise, but the wise man knows himself to be a fool.",
    name: "Shakespeare",
  },
  {
    id: 10,
    quote:
      "Whenever you find yourself on the side of the majority, it is time to reform (or pause and reflect).",
    name: "Mark Twain",
  },
];

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type QuoteItem {
    id: Int
    quote: String
    name: String
  }

  type Query {
    quote: QuoteItem
    user : User
  }

  type User {
    id: Int
    firstName: String
    lastName: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  quote: () => {
    return quotes[Math.floor(Math.random() * 10)];
  },
  user: async () => {
    const response = await axios.request({
      method: "get",
      url: "https://my-json-server.typicode.com/atothey/demo/user",
    });
    return response.data;
  },
};

var app = express();
app.use(
  cors({
    origin: "http://localhost:5001",
  })
);
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);
app.listen(PORT);
console.log("Running GraphQL API server!!");
