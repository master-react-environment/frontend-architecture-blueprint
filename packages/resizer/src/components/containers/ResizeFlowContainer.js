import React, { useCallback } from "react";
import { MarkerType, Position, useReactFlow } from "react-flow-renderer";

import ResizeFlowPanel from "../panels/ResizeFlowPanel";
import { ResizeRotateNodeContainer as FunctionComponent } from "./ResizeRotateNodeContainer";

const nodes = [
  {
    id: "1",
    position: { x: 100, y: 100 },
    data: {
      text: "I am an editable, resizable and rotatable node.",
      color: "#ff0072",
    },
    type: "resizeRotate",
    sourcePosition: Position.Right,
    targetPosition: Position.Left,
  },
  {
    id: "2",
    position: { x: 500, y: 300 },
    data: { text: "Click here to edit...", color: "#ff6700", rotation: 20 },
    type: "resizeRotate",
    sourcePosition: Position.Bottom,
    targetPosition: Position.Top,
  },
  {
    id: "3",
    position: { x: 100, y: 500 },
    data: {
      text: "Another node that can be resized...",
      color: "#784be8",
      rotation: 20,
    },
    type: "resizeRotate",
    sourcePosition: Position.Left,
    targetPosition: Position.Right,
  },
];

const edges = [
  {
    id: "1->2",
    source: "1",
    target: "2",
    type: "smoothstep",
  },
  {
    id: "2->3",
    source: "2",
    target: "3",
    type: "smoothstep",
  },
];

const nodeTypes = {
  resizeRotate: FunctionComponent,
};

const defaultEdgeOptions = {
  style: { strokeWidth: 3, stroke: "#9ca8b3" },
  markerEnd: {
    type: MarkerType.ArrowClosed,
  },
  zIndex: 1,
};

const proOptions = { account: "paid-pro", hideAttribution: true };

const ResizeFlowContainer = () => {
  const { getNodes, setNodes } = useReactFlow();

  const onMoveStart = useCallback(() => {
    const nodes = getNodes().map((n) => {
      n.selected = false;
      return n;
    });

    setNodes(nodes);
  }, [getNodes,setNodes]);

  return (
    <ResizeFlowPanel
      proOptions={proOptions}
      onMoveStart={onMoveStart}
      defaultEdgeOptions={defaultEdgeOptions}
      nodes={nodes}
      edges={edges}
      nodeTypes={nodeTypes}
    />
  );
};

export default ResizeFlowContainer;
