import React, { useEffect, useState, useRef, useMemo } from "react";
import Moveable from "moveable";
import { useUpdateNodeInternals } from "react-flow-renderer";
import { ResizeRotateNodePanel } from "../panels/ResizeRotateNodePanel";

export const ResizeRotateNodeContainer = ({
  id,
  data,
  selected,
  dragging,
  sourcePosition,
  targetPosition,
}) => {
  const nodeRef = useRef();
  const [size, setSize] = useState({ width: 300, height: 150 });
  const [transform, setTransform] = useState("none");
  const updateNodeInternals = useUpdateNodeInternals();

  const style = useMemo(
    () => ({
      transform,
      width: size.width,
      height: size.height,
      background: data?.color,
      padding: 20,
      borderRadius: 20,
    }),
    [transform, size.width, size.height, data?.color]
  );

  useEffect(() => {
    if (!nodeRef.current || !selected || dragging) {
      return;
    }

    const moveable = new Moveable(document.body, {
      target: nodeRef.current,
      className: "nodrag",
      draggable: false,
      resizable: true,
      scalable: false,
      rotatable: true,
      warpable: false,
      pinchable: false,
      origin: false,
      keepRatio: false,
      edge: false,
      throttleDrag: 0,
      throttleResize: 0,
      throttleScale: 0,
      throttleRotate: 0,
      dragArea: false,
    });

    moveable.on("rotate", ({ transform }) => {
      setTransform(transform);
    });

    moveable.on("resize", ({ width, height, drag }) => {
      setTransform(drag.transform);
      setSize({ width, height });
    });

    return () => moveable.destroy();
  }, [selected, dragging]);

  useEffect(() => {
    updateNodeInternals(id);
  }, [transform, id, updateNodeInternals]);

  return (
    <ResizeRotateNodePanel
      nodeRef={nodeRef}
      data={data}
      style={style}
      sourcePosition={sourcePosition}
      targetPosition={targetPosition}
    />
  );
};
