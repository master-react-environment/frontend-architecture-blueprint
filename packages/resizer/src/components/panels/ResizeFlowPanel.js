import React from "react";
import ReactFlow, { Background } from "react-flow-renderer";

const ResizeFlowPanel = ({
  proOptions,
  onMoveStart,
  defaultEdgeOptions,
  nodes,
  edges,
  nodeTypes,
}) => {
  return (
    <ReactFlow
      nodeTypes={nodeTypes}
      defaultNodes={nodes}
      defaultEdges={edges}
      defaultEdgeOptions={defaultEdgeOptions}
      onMoveStart={onMoveStart}
      proOptions={proOptions}
    >
      <Background />
    </ReactFlow>
  );
};

export default ResizeFlowPanel;
