import React from "react";
import { ReactFlowProvider } from "react-flow-renderer";
import ResizeFlowContainer from "../containers/ResizeFlowContainer";

export const ReactFlowWrapper = (props) => {
  return (
    <ReactFlowProvider>
      <ResizeFlowContainer {...props} />
    </ReactFlowProvider>
  );
};

export default ReactFlowWrapper;
