import React from "react";
import { Handle } from "react-flow-renderer";

export const ResizeRotateNodePanel = ({
  nodeRef,
  data,
  style,
  sourcePosition,
  targetPosition,
}) => {
  return (
    <div ref={nodeRef} style={style}>
      <textarea
        style={{
          width: "100%",
          height: "100%",
          resize: "none",
          backgroundColor: "rgba(255, 255, 255, 0)",
          fontWeight: "bold",
          color: "white",
          padding: 5,
          fontSize: 22,
          boxSizing: "border-box",
          outline: "none",
          border: "none",
        }}
        defaultValue={data?.text}
      />
      <Handle style={{ opacity: 0 }} position={sourcePosition} type="source" />
      <Handle style={{ opacity: 0 }} position={targetPosition} type="target" />
    </div>
  );
};
