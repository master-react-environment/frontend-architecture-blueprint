import ResizeFlowWrapperPanel from "./components/panels/ResizeFlowWrapperPanel";

function App() {
  return (
    <div style={{ width: "100%", height: "100vh" }}>
      <ResizeFlowWrapperPanel />
    </div>
  );
}

export default App;
