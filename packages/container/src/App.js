import React, { Suspense } from "react";
import ErrorBoundary from "./ErrorBoundary";

const DesignerMFEApp = React.lazy(() => import("DesignerMFE/App"));
const ResizerMFEApp = React.lazy(() => import("ResizerMFE/App"));

const RemoteWrapper = ({ children }) => (
  <div
    style={{
      border: "1px solid red",
      background: "white",
    }}
  >
    <ErrorBoundary>{children}</ErrorBoundary>
  </div>
);

export const App = () => {
  return (
    <div>
      <Suspense fallback={<div>Loading... </div>}>
        <RemoteWrapper>
          <DesignerMFEApp />
        </RemoteWrapper>
        <RemoteWrapper>
          <ResizerMFEApp />
        </RemoteWrapper>
      </Suspense>
    </div>
  );
};
