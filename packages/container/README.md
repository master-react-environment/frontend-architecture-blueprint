For writing test cases, refer `https://redux.js.org/usage/writing-tests#components`.

For creating CRA app with PWA (using npm), run `npx create-react-app my-app --template cra-template-pwa`.

For creating CRA app with PWA (using yarn), run `yarn create react-app my-app --template cra-template-pwa`.
