import DnDFlowContainer from "./components/containers/dragAndDrop/DnDFlowContainer";

function App() {
  return (
    <div style={{ width: "100%", height: "100vh" }}>
      <DnDFlowContainer />
    </div>
  );
}

export default App;
