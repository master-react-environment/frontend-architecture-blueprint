import { ReactComponent as SVG1 } from '../assets/svg1.svg';
import { ReactComponent as SVG2 } from '../assets/svg2.svg';
import { ReactComponent as SVG3 } from '../assets/svg3.svg';

const localSvgs = {
    'svg1':SVG1,
    'svg2':SVG2,
    'svg3':SVG3      
};

export default localSvgs;