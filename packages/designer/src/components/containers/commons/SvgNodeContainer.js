import { useSelector } from "react-redux";
import SvgNodePanel from "../../panels/commons/SvgNodePanel";

function SvgNodeContainer({ data }) {
  // =========== Legacy vs RTK =========== //
  // const textValueState = useSelector(state => state.value);
  const textValueState = useSelector((state) => state.valueReducer.input);
  // =========== Legacy vs RTK =========== //

  console.log("textValueState", textValueState);
  let textValue = "";

  switch (data.type) {
    case "svg1":
      if (textValueState >= 100 && textValueState <= 105) {
        textValue = "yellow";
      }
      if (textValueState > 105) {
        textValue = "red";
      }
      break;
    case "svg2":
      if (textValueState >= 106 && textValueState <= 110) {
        textValue = "yellow";
      }
      if (textValueState > 110) {
        textValue = "red";
      }
      break;
    case "svg3":
      if (textValueState >= 111 && textValueState <= 115) {
        textValue = "yellow";
      }
      if (textValueState > 115) {
        textValue = "red";
      }
      break;
    default:
      textValue = "";
  }

  return <SvgNodePanel machine={data.machine} textValue={textValue} />;
}

export default SvgNodeContainer;
