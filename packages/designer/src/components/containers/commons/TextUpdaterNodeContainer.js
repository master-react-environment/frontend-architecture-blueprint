import { useCallback } from 'react';


import { useDispatch} from 'react-redux';
import TextUpdaterNodePanel from '../../panels/commons/TextUpdaterNodePanel';


// =========== Legacy vs RTK =========== //
// import {valueChangeAction } from '../../../stores/actions';
import {valueChange } from '../../../stores/store/slices/valueSlice';
// =========== Legacy vs RTK =========== //

const handleStyle = { left: 10 };

function TextUpdaterNodeContainer({ data }) {

    const dispatch = useDispatch();
    const setTextValue = useCallback((val) =>{
      // =========== Legacy vs RTK =========== //
        // dispatch(valueChangeAction(val))
        dispatch(valueChange(val))    
      // =========== Legacy vs RTK =========== //    
    },[dispatch]);
  
  const onChange = useCallback((evt) => {
    setTextValue(evt.target.value)
  }, [setTextValue]);

  return (    
    <TextUpdaterNodePanel onChange={onChange} handleStyle={handleStyle} />
  );
}

export default TextUpdaterNodeContainer;