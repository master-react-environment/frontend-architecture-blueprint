import React, { useState, useRef, useCallback } from 'react';
import  {  
  addEdge,
  useNodesState,
  useEdgesState  
} from 'react-flow-renderer';

import  Machines from '../../../utils/image-manager'

import TextUpdaterNodeContainer from '../../containers/commons/TextUpdaterNodeContainer';
import SvgNodeContainer from '../../containers/commons/SvgNodeContainer';

import '../../../styles/commons/TextUpdaterNode/text-updater-node.css';
import '../../../styles/dragAndDrop/DnDFlow/dnd-flow.css';
import DnDFlowPanel from '../../panels/dragAndDrop/DnDFlowPanel';

const initialNodes = [
 
];

let id = 0;
const getId = () => `dndnode_${id++}`;

const nodeTypes = { controller: TextUpdaterNodeContainer, machine:SvgNodeContainer };

const DnDFlowContainer = () => {
  const reactFlowWrapper = useRef(null);

  const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState([]);


  const onConnect = useCallback(
    (connection) => setEdges((eds) => addEdge(connection, eds)),
    [setEdges]
  );

  const [reactFlowInstance, setReactFlowInstance] = useState(null);

  // const onConnect = useCallback((params) => setEdges((eds) => addEdge(params, eds)), []);

  const onDragOver = useCallback((event) => {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }, []);

  const onDrop = useCallback(
    (event) => {
      event.preventDefault();

      const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
      // const type = event.dataTransfer.getData('application/reactflow');

      const rdObject = JSON.parse(event.dataTransfer.getData('application/reactflow'));
      const type = rdObject.type;
      console.log(rdObject.machineType)
      // check if the dropped element is valid
      if (typeof type === 'undefined' || !type) {
        return;
      }

      const position = reactFlowInstance.project({
        x: event.clientX - reactFlowBounds.left,
        y: event.clientY - reactFlowBounds.top,
      });

      let nodeData;
      if(rdObject.machineType === 'controller'){
        nodeData={}
      } else {
        nodeData= {machine:Machines[rdObject.machineType], type:rdObject.machineType}
      }

      const newNode = {
        id: getId(),
        type,
        position,
        data: nodeData,
      };
      setNodes((nds) => nds.concat(newNode));
    },
    [reactFlowInstance,setNodes]
  );
  return (
    <DnDFlowPanel 
        onDragOver={onDragOver}
        onDrop={onDrop}
        onConnect={onConnect}
        setReactFlowInstance={setReactFlowInstance}
        nodes={nodes}
        edges={edges}
        onNodesChange={onNodesChange}
        onEdgesChange={onEdgesChange}
        reactFlowWrapper={reactFlowWrapper}
        nodeTypes={nodeTypes}    
    />
  );
};

export default DnDFlowContainer;