import React from 'react';
import SidebarPanel from '../../panels/dragAndDrop/SidebarPanel';

const SidebarContainer = () => {
  const onDragStart = (event, nodeType,machineType) => {
    let txObject = JSON.stringify({
      type:nodeType,
      machineType
    })
    event.dataTransfer.setData('application/reactflow', txObject);
    event.dataTransfer.effectAllowed = 'move';
  };



  return (
    <SidebarPanel onDragStart={onDragStart} />
  );
};

export default SidebarContainer;