import { useQuery } from "@apollo/client";
import QuoteItemPanel from "../../panels/quotes/QuoteItemPanel";
import { GET_QUOTE } from "../../../graphqlConstants/quotes";

export default function QuoteItemContainer() {
  const { loading, error, data } = useQuery(GET_QUOTE);

  return <QuoteItemPanel loading={loading} error={error} data={data} />;
}
