import { useEffect } from 'react';
import { useSelector , useDispatch} from 'react-redux';
import QuoteCardPanel from '../../panels/quotes/QuoteCardPanel';
// =========== Legacy vs RTK =========== //
// import {getUser} from '../../../stores/actions';
import {getUser} from '../../../stores/store/slices/userSlice';
// =========== Legacy vs RTK =========== //

const QuoteCardContainer = () => {
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(getUser());
    },[dispatch]);

    const user = useSelector((state) => state.user.user);
    console.log(user)
    return (
        <QuoteCardPanel user={user.firstName} />        
    );
};

export default QuoteCardContainer;