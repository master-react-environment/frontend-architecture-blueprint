import { Handle, Position } from 'react-flow-renderer';


function SvgNodePanel({ machine, textValue }) {

  const getMachine=()=>{
    const NodeMachine=machine;
    console.log(NodeMachine)
    return <NodeMachine width="3rem" height="3rem" fill={textValue} stroke={textValue} />
  }

  return (
    <div>
      <div >
        {getMachine()}
      </div>
      <Handle type="target" position={Position.Bottom} />
    </div>
  );
}

export default SvgNodePanel;