import { Handle, Position } from 'react-flow-renderer';

function TextUpdaterNodePanel({onChange,handleStyle}) {

  return (    
    <div className="text-updater-node">
      <Handle type="source" position={Position.Top} />
      <div>
        <label htmlFor="text">Enter value to modify SVG:</label>
        <input id="text" name="text" onChange={onChange} />
      </div>
      <Handle type="source" position={Position.Bottom} id="a" style={handleStyle} />
      <Handle type="source" position={Position.Bottom} id="b" />
    </div>
  );
}

export default TextUpdaterNodePanel;