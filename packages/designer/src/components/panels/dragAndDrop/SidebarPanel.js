import React from 'react';

import  Machines from '../../../utils/image-manager';

import QuoteCardContainer from '../../containers/quotes/QuoteCardContainer';

const SidebarPanel =  ({ onDragStart }) => {
 
  const Machine1 = Machines['svg1'];
  const Machine2 = Machines['svg2'];
  const Machine3 = Machines['svg3'];
  

  return (
    <aside>
      <div className="description">SVG components</div>     
      <div className="dndnode" onDragStart={(event) => onDragStart(event, 'controller')} draggable>
        Controller Node
      </div>     
      <div  onDragStart={(event) => onDragStart(event, 'machine','svg1')} draggable>
        <Machine1 width="3rem" height="3rem" />
      </div>      
      <div  onDragStart={(event) => onDragStart(event, 'machine','svg2')} draggable>
        <Machine2 width="3rem" height="3rem" />
      </div>
      <div  onDragStart={(event) => onDragStart(event, 'machine','svg3')} draggable>
        <Machine3 width="3rem" height="3rem" />
      </div>
      <br />
      <br />
      <QuoteCardContainer />
    </aside>
  );
};

export default SidebarPanel;