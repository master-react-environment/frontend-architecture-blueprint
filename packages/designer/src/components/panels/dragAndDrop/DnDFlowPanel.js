import React from 'react';
import ReactFlow, {
  ReactFlowProvider,
  Controls,
} from 'react-flow-renderer';

import SidebarContainer from '../../containers/dragAndDrop/SidebarContainer';

const DnDFlowPanel = ({onDragOver,onDrop,onConnect,nodes,edges,onNodesChange,onEdgesChange,reactFlowWrapper, nodeTypes, setReactFlowInstance}) => { 
  return (
    <div className="dndflow">
      
      <ReactFlowProvider>
        
        <div className="reactflow-wrapper" ref={reactFlowWrapper}>
          <ReactFlow
            nodes={nodes}
            edges={edges}
            onNodesChange={onNodesChange}
            onEdgesChange={onEdgesChange}
            onConnect={onConnect}
            onInit={setReactFlowInstance}
            onDrop={onDrop}
            onDragOver={onDragOver}
            nodeTypes={nodeTypes}
            fitView
            proOptions={{account:"paid-enterprise",hideAttribution:true}}
          >
            <Controls />
          </ReactFlow>
        </div>
        <SidebarContainer />
        
      </ReactFlowProvider>
      
    </div>
  );
};

export default DnDFlowPanel;