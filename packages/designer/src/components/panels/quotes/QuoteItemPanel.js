export default function QuoteItemPanel({loading, error, data}) {
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
    console.log(data.quote);
    return (
        <div key={data.quote.id}>
            <h2>{data.quote.quote}</h2>
            <h4> - {data.quote.name}</h4>
        </div>
    );
}