import QuoteItemContainer from "../../containers/quotes/QuoteItemContainer";


const QuoteCardPanel = ({user}) => {
    
    return (
        <div>
            <h1>Hi, {user}</h1>
            <QuoteItemContainer />
        </div>
    );
};

export default QuoteCardPanel;