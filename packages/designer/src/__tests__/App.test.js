import { screen } from "@testing-library/react";
import { renderWithProviders } from "./utils/store-utils";
import App from "../App";

// jest.mock("use-resize-observer", () => ({
//   __esModule: true,
//   default: jest.fn().mockImplementation(() => ({
//     observe: jest.fn(),
//     unobserve: jest.fn(),
//     disconnect: jest.fn(),
//   })),
// }));

class ResizeObserver {
  observe = jest.fn();
  unobserve = jest.fn();
  disconnect = jest.fn();
}
window.ResizeObserver = ResizeObserver;

test("renders the landing page", () => {
  renderWithProviders(<App />);
  expect(screen.getByText(/Hi/i)).toBeInTheDocument();
});
