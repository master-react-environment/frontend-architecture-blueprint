import { combineReducers, configureStore } from "@reduxjs/toolkit";

import valueReducer from "../../stores/store/slices/valueSlice";
import userReducer from "../../stores/store/slices/userSlice";

const rootReducer = combineReducers({
  valueReducer: valueReducer,
  user: userReducer,
});

export const setupStore = (preloadedState) => {
  return configureStore({
    reducer: rootReducer,
    preloadedState,
  });
};
