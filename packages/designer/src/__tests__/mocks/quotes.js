import { GET_QUOTE } from "../../graphqlConstants/quotes";

export const quotesMocks = [
  {
    request: {
      query: GET_QUOTE,

    },

    result: {
      data: {
        quote: { id: "1", quote: "Life's Good", name: "LG" },
      },
    },
  },
];
