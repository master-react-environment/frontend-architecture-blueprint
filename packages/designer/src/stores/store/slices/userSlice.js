import { createSlice } from '@reduxjs/toolkit'

const userSlice = createSlice({
  name: 'userSlice',
  initialState: {
    user: ''
  },
  reducers: {
    getUser(){},
    setUser: (state, action) => {
      state.user = action.payload      
    }    
  }
})

//Generated action creator
export const { getUser, setUser } = userSlice.actions
//Reducer function to be used to create the store
export default userSlice.reducer