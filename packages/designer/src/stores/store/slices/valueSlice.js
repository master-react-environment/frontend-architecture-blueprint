import { createSlice } from '@reduxjs/toolkit'

const valueSlice = createSlice({
  name: 'controllerValue',
  initialState: {
    input: 0
  },
  reducers: {
    valueChange: (state, action) => {
      state.input = action.payload      
    }    
  }
})

//Generated action creator
export const { valueChange } = valueSlice.actions
//Reducer function to be used to create the store
export default valueSlice.reducer