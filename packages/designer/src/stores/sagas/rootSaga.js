import {takeLatest} from "redux-saga/effects";
import {handleGetUser} from './handlers/user';
// =========== Legacy vs RTK =========== //
// import Types from '../actionTypes'; 
import { getUser } from "../store/slices/userSlice";
// =========== Legacy vs RTK =========== //

export function* watcherSaga(){
    // =========== Legacy vs RTK =========== //
    // yield takeLatest(Types.GET_USER, handleGetUser)
    yield takeLatest(getUser.type, handleGetUser)
    // =========== Legacy vs RTK =========== //
}