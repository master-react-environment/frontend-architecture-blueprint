import {call , put} from "redux-saga/effects";
// =========== Legacy vs RTK =========== //
// import { setUser } from "../../actions";
import { setUser } from "../../store/slices/userSlice";
// =========== Legacy vs RTK =========== //
import {requestGetUser} from "../requests/user"

export function* handleGetUser(){
    try{
        const response = yield call(requestGetUser);
        const {data} = response;
        // =========== Legacy vs RTK =========== //
        // yield put(setUser(data));
        yield put(setUser({...data}));
        // =========== Legacy vs RTK =========== //
    }catch(error){
        console.log(error);
    }
}