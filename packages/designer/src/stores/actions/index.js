import Types from '../actionTypes';

export const valueChangeAction = val => {
    return {
        type: Types.ON_VALUE_CHANGE,
        payload: val
    }
};

export const getUser = () => ({
    type: Types.GET_USER
});

export const setUser = (user) => ({
    type: Types.SET_USER,
    user
});
