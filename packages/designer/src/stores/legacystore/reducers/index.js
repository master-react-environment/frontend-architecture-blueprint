import valueChangeReducer from './valueChangeReducer';
import userReducer from './userReducer';


import {combineReducers} from 'redux';

const combinedReducers = combineReducers({
    value: valueChangeReducer,
    user: userReducer
});

export default combinedReducers;