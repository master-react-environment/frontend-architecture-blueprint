import Types from '../../actionTypes';
const valueChangeReducer = (state=0,action)=>{
    switch(action.type){
        case Types.ON_VALUE_CHANGE:
            return action.payload;
        default:
            return state
    }
}

export default valueChangeReducer;