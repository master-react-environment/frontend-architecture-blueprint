import Types from '../../actionTypes';

const initialState = {
    user: ''
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.SET_USER:
            const { user } = action;
            return { ...state, user };
        default:
            return state;
    }
};

export default userReducer;