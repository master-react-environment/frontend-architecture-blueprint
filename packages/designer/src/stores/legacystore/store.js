import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from "redux-saga";
import { watcherSaga } from '../sagas/rootSaga';

import combinedReducers from './reducers';

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

const store = createStore(combinedReducers,{},applyMiddleware(...middleware));

sagaMiddleware.run(watcherSaga);

export default store;

