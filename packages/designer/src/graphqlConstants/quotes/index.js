import { gql } from "@apollo/client";

export const GET_QUOTE = gql`
  query QuoteItem {
    quote {
      id
      quote
      name
    }
  }
`;
