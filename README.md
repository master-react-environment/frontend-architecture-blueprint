This is a lerna monorepo which comprises of a container PWA app and two microfrontends (stitched together in the container app) and a sample graphql backend for supporting the frontend network calls. This repo utilizes all the best and performant stacks which was decided after careful deliberations. Also, the organization of the code is made keeping in mind the standard coding practices. Hence, this repo can be used as a blueprint for any future React project developments.

_Lerna is used here for the ease of demoing._

**Usage**

After cloning in the repo, run `yarn` in the root directory to install all the dependencies. Post the completion of this command, run `yarn start` in the root directory itself to start all the applications in one go.

**Demo Application**

There are two microfrontends to the main application. The first one is called Designer where you can drag and drop in SVG icons and modify them using user input entered via a text box (another node that needs to be dropped in). The second one is called Resizer which basically demoes how we can resize and move nodes on a playground.

The Designer and Resizer is stitched together into the container app. The container app is a PWA rendering the microfrontends.

**Technology Info**

- Architecture style: Microfrontend
- Type: PWA (Progressive Web app)
- Frontend: React
- State Management: Redux toolkit (RTK)
- Side Effect Management: Redux Saga for Redux
- Network Calls: GraphQL with Apollo GraphQL as the provider
- Testing: Jest with testing-library
- Coding standards: ESLint and Prettier (at a minimum these 2 are mandatory).

- _Demo application libraries: React Flow, Moveable_

Since Facebook itself has strictly recommended the use of RTK instead of classic Redux, the app has adopted RTK for global state management.

For comparison and reference purposes, I have ceated two stores (`store` based on RTK and `legacystore` based on classic Redux). Currently teh app is working on `store`. In order to make the app work on `legacystore`, find the blocks of code wrapped with `// =========== Legacy vs RTK =========== //` and toggle the commented codes with the uncommented ONES in the block and vice-versa. Restarting the application after the code change, will make the app run on classic Redux (with Saga, ofcourse).

**URLs**

- Container app: http://localhost:5000
- Microfrontend 1: http://localhost:5001
- Microfrontend 2: http://localhost:5002
- Sample GraphQL server: http://localhost:4000/graphql

**Reference Guides**

For creating CRA app with PWA, run `npx create-react-app my-app --template cra-template-pwa`.

For writing test cases, refer `https://redux.js.org/usage/writing-tests#components`.
